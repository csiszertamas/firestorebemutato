package com.google.firebase.example.fireeats.util;

import android.content.Context;

import com.google.firebase.example.fireeats.R;
import com.google.firebase.example.fireeats.model.Hotel;

import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class HotelUtil {

    private static final String TAG = "HotelUtil";

    private static final ThreadPoolExecutor EXECUTOR = new ThreadPoolExecutor(2, 4, 60,
            TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>());

    private static final int MAX_IMAGE_NUM = 10;

    private static final String[] NAME_FIRST_WORDS = {
            "Hilton",
            "Royal",
            "Countryside's",
            "John's",
            "Weston",
            "Sam's",
            "World Famous",
            "Ritz-Carlton",
            "The Best",
    };

    private static final String[] NAME_SECOND_WORDS = {
            "Hotel",
            "Resort",
            "Motel",
            "Resort & Spa",
            "Hotel & Spa",
            "Mansion",
            "Residence",
    };


    public static Hotel getRandom(Context context) {
        Hotel hotel = new Hotel();
        Random random = new Random();

        // Cities (first elemnt is 'Any')
        String[] cities = context.getResources().getStringArray(R.array.cities);
        cities = Arrays.copyOfRange(cities, 1, cities.length);

        // Categories (first element is 'Any')
        String[] categories = context.getResources().getStringArray(R.array.categories);
        categories = Arrays.copyOfRange(categories, 1, categories.length);

        int[] prices = new int[]{1, 2, 3};

        hotel.setName(getRandomName(random));
        hotel.setCity(getRandomString(cities, random));
        hotel.setCategory(getRandomString(categories, random));
        hotel.setPhoto(getRandomImageUrl(random));
        hotel.setPrice(getRandomInt(prices, random));
        hotel.setAvgRating(getRandomRating(random));
        hotel.setNumRatings(random.nextInt(20));

        return hotel;
    }


    /**
     * Get a random image.
     */
    private static String getRandomImageUrl(Random random) {
        // Integer between 1 and MAX_IMAGE_NUM (inclusive)
        int id = random.nextInt(MAX_IMAGE_NUM) + 1;

        switch (id) {
            case 1:
                return "https://firebasestorage.googleapis.com/v0/b/firestorebemutato.appspot.com/o/hotels_images%2F1.jpg?alt=media&token=8ed65d4a-f4c2-448a-8ba0-8b553c386ad0";
            case 2:
                return "https://firebasestorage.googleapis.com/v0/b/firestorebemutato.appspot.com/o/hotels_images%2F2.jpg?alt=media&token=4318305e-ef84-43a7-a91e-1ea101d43c46";
            case 3:
                return "https://firebasestorage.googleapis.com/v0/b/firestorebemutato.appspot.com/o/hotels_images%2F3.jpg?alt=media&token=49f061df-db45-425d-adb5-f1011ec37763";
            case 4:
                return "https://firebasestorage.googleapis.com/v0/b/firestorebemutato.appspot.com/o/hotels_images%2F4.jpg?alt=media&token=dfade087-f41d-40cb-b63b-12fd1164c99f";
            case 5:
                return "https://firebasestorage.googleapis.com/v0/b/firestorebemutato.appspot.com/o/hotels_images%2F5.jpg?alt=media&token=62218824-0782-4434-b63d-15c48bfefd7c";
            case 6:
                return "https://firebasestorage.googleapis.com/v0/b/firestorebemutato.appspot.com/o/hotels_images%2F6.jpg?alt=media&token=5fde2628-ffd2-45ee-a7a9-87c18d3884f7";
            case 7:
                return "https://firebasestorage.googleapis.com/v0/b/firestorebemutato.appspot.com/o/hotels_images%2F7.jpg?alt=media&token=a8ed4699-fca9-42e6-88a6-0631e96cf950";
            case 8:
                return "https://firebasestorage.googleapis.com/v0/b/firestorebemutato.appspot.com/o/hotels_images%2F8.jpg?alt=media&token=09d67063-3c4f-4efd-aa90-2fc7e04d957a";
            case 9:
                return "https://firebasestorage.googleapis.com/v0/b/firestorebemutato.appspot.com/o/hotels_images%2F9.jpg?alt=media&token=b36dc906-dcfb-4019-a007-b145ba9d6357";
            case 10:
                return "https://firebasestorage.googleapis.com/v0/b/firestorebemutato.appspot.com/o/hotels_images%2F10.jpg?alt=media&token=7303698b-c7c8-487b-9fc0-bbf6c1ad360e";
        }

        return "https://firebasestorage.googleapis.com/v0/b/firestorebemutato.appspot.com/o/1.jpg?alt=media&token=b43f5770-365c-434e-ab73-b7d45628436f";
    }

    /**
     * Get price represented as dollar signs.
     */
    public static String getPriceString(Hotel hotel) {
        return getPriceString(hotel.getPrice());
    }

    /**
     * Get price represented as dollar signs.
     */
    public static String getPriceString(int priceInt) {
        switch (priceInt) {
            case 1:
                return "$";
            case 2:
                return "$$";
            case 3:
            default:
                return "$$$";
        }
    }

    private static double getRandomRating(Random random) {
        double min = 1.0;
        return min + (random.nextDouble() * 4.0);
    }

    private static String getRandomName(Random random) {
        return getRandomString(NAME_FIRST_WORDS, random) + " "
                + getRandomString(NAME_SECOND_WORDS, random);
    }

    private static String getRandomString(String[] array, Random random) {
        int ind = random.nextInt(array.length);
        return array[ind];
    }

    private static int getRandomInt(int[] array, Random random) {
        int ind = random.nextInt(array.length);
        return array[ind];
    }


}
