package com.google.firebase.example.fireeats.adapter;

import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.example.fireeats.R;
import com.google.firebase.example.fireeats.model.Hotel;
import com.google.firebase.example.fireeats.util.HotelUtil;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.Query;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.zhanghai.android.materialratingbar.MaterialRatingBar;

public class HotelAdapter extends FirestoreAdapter<HotelAdapter.ViewHolder> {

    private OnHotelSelectedListener mListener;

    public HotelAdapter(Query query, OnHotelSelectedListener listener) {
        super(query);
        mListener = listener;
    }

    public interface OnHotelSelectedListener {

        void onHotelSelected(DocumentSnapshot hotel);

        void onHotelLongTapped(DocumentSnapshot hotel);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        return new ViewHolder(inflater.inflate(R.layout.item_hotel, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(getSnapshot(position), mListener);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        //region ViewHolder UI
        @BindView(R.id.hotel_item_image)
        ImageView imageView;

        @BindView(R.id.hotel_item_name)
        TextView nameView;

        @BindView(R.id.hotel_item_rating)
        MaterialRatingBar ratingBar;

        @BindView(R.id.hotel_item_num_ratings)
        TextView numRatingsView;

        @BindView(R.id.hotel_item_price)
        TextView priceView;

        @BindView(R.id.hotel_item_category)
        TextView categoryView;

        @BindView(R.id.hotel_item_city)
        TextView cityView;
        //endregion

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(final DocumentSnapshot snapshot,
                         final OnHotelSelectedListener listener) {

            Hotel hotel = snapshot.toObject(Hotel.class);
            Resources resources = itemView.getResources();

            // Load image
            Glide.with(imageView.getContext())
                    .load(hotel.getPhoto())
                    .into(imageView);

            nameView.setText(hotel.getName());
            ratingBar.setRating((float) hotel.getAvgRating());
            cityView.setText(hotel.getCity());
            categoryView.setText(hotel.getCategory());
            numRatingsView.setText(resources.getString(R.string.fmt_num_ratings,
                    hotel.getNumRatings()));
            priceView.setText(HotelUtil.getPriceString(hotel));

            // Click listener
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        listener.onHotelSelected(snapshot);
                    }
                }
            });

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    if (listener != null) {
                        listener.onHotelLongTapped(snapshot);
                        return true;
                    }
                    return false;
                }
            });
        }

    }

}
